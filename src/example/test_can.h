#ifndef __OPENLOONGSON_TEST_CAN_H
#define __OPENLOONGSON_TEST_CAN_H
void test_can_general(void);
void test_can_irqhandler(int IRQn, void *param);
void test_can_irq(void);

#endif

