#include "../lib/ls1c_public.h"
#include "../lib/ls1c_gpio.h"
#include "../lib/ls1c_delay.h"
#include "../lib/ls1b_can.h"
#include "../lib/ls1c_irq.h"
#include "../lib/ls1c_mipsregs.h"
#include "../lib/ls1c_regs.h"
#include "test_can.h"
// 按键中断的标志
volatile int can_irq_flag = 0;
void test_can_general(void)
{
	int i;
	myprintf("==========000000000\n");
	volatile unsigned char* base0;
	base0= LS1B_REG_BASE_CAN0;
	volatile unsigned char* base1;
	base1= LS1B_REG_BASE_CAN1;

	
	//*(volatile unsigned int *)(0xbfd010c4)&=~(15<<6);
	
 /*   printf("**********************\n");           
	printf("0xbfd010c4:%x\n",*(volatile unsigned int*)0xbfd010c4);
	printf("0xbfd00420:%x\n",*(volatile unsigned int*)0xbfd00420);
	printf("0xbfd00424:%x\n",*(volatile unsigned int*)0xbfd00424);
    #define PLL_FREQ_REG(x) *(volatile unsigned int*)(0xbfe78030+x)
	printf("****pll=0x%x*****ctrl=0x%x*****\n",PLL_FREQ_REG(0),PLL_FREQ_REG(4));	
*/	
//	dumpcanregs(base0);
//	myprintf("==========111111\n");
//	dumpcanregs(base1);
	CANInit(base0);	
//	CANInit(base1);	

//	dumpcanregs(base0);
//	myprintf("==========222222\n");
//	dumpcanregs(base1);

	for( i=0;i<5;i++){

	//send_frame(base0);
	myprintf("==========333333\n");
	receive_frame(base0);
//	send_frame(base1);

	}
}	


/*
 * 按键中断的处理函数
 * @IRQn 中断号
 * @param 传递给中断处理函数的参数
 */
void test_can_irqhandler(int IRQn, void *param)
{
	volatile unsigned char *base =(volatile unsigned char *)param;
	receive_frame(base);
	can_irq_flag = 1;
	//myprintf("\n****\n");
	//*(base+1)=0x8c;
}

/*
 * 测试库中外部中断(gpio中断，按键中断)的相关接口
 * 按键被按下后，会产生一个中断
 */
void test_can_irq(void)
{
	static int count = 0;
	myprintf("\n\n*****Welcome to CHINA*****\n\n");
	volatile unsigned char* base0;
	base0= LS1B_REG_BASE_CAN0;
	volatile unsigned char* base1;
	base1= LS1B_REG_BASE_CAN1;
	CANInit(base0);	
  
	irq_install(LS1B_CAN0_IRQ, test_can_irqhandler, (void *)base0);
	receive_irq_enable(base0);
	irq_enable(LS1B_CAN0_IRQ);
	dumpcanregs(base0);
    while (1)
    {
		myprintf("\n*********\n");
        delay_s(2);
		if (1 == can_irq_flag)
        {
            can_irq_flag = 0;      // 清标志
            count++;                    // 计数器加一
            myprintf("[%s] count=%d \n", __FUNCTION__, count);
	//		receive_frame(base0);
        }
    }
}



