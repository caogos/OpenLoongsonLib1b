#ifndef __OPENLOONGSON_CAN_H
#define __OPENLOONGSON_CAN_H

// CAN引脚定义
#define LS1B_CAN0_RX_GPIO38                    (38)        
#define LS1B_CAN0_TX_GPIO39                    (39)       
#define LS1B_CAN1_RX_GPIO40                   (40)      
#define LS1B_CAN1_TX_GPIO41                   (41)        

// ...还有一些gpio可以复用为gpio的，有需要可以自己添加
void dumpcanregs(volatile unsigned char* base);
void bustimer_init(volatile unsigned char* base);
void desc_init(volatile unsigned char* base);
void set_mode(volatile unsigned char* base);
void CANInit(volatile unsigned char* base);
void send_frame(volatile unsigned char* base);
void receive_frame(volatile volatile unsigned char* base);
void receive_irq_enable(volatile unsigned char* base);


#endif
