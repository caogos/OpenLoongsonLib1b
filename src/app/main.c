#include "../lib/ls1c_public.h"
#include "../lib/ls1c_gpio.h"
#include "../lib/ls1c_delay.h"
#include "../lib/ls1c_mipsregs.h"
#include "../lib/ls1c_irq.h"
#include "../lib/ls1c_sys_tick.h"
#include "../example/test_gpio.h"
#include "../example/test_pwm.h"
#include "../example/test_delay.h"
#include "../example/test_simulate_i2c.h"
#include "../example/test_timer.h"
#include "../example/test_fpu.h"
#include "../example/test_i2c.h"
#include "../example/test_can.h"

// pmon提供的打印接口
struct callvectors *callvec;
extern void general_exception(void);
extern void irq_disable_all(void);
extern void irq_enable_all(void);
// 硬浮点初始化
void fpu_init(void)
{
    unsigned int c0_status = 0;
    unsigned int c1_status = 0;

    // 使能协处理器1--FPU
    c0_status = read_c0_status();
    c0_status |= (ST0_CU1 | ST0_FR);
    write_c0_status(c0_status);

    // 配置FPU
    c1_status = read_c1_status();
    c1_status |= (FPU_CSR_FS | FPU_CSR_FO | FPU_CSR_FN);    // set FS, FO, FN
    c1_status &= ~(FPU_CSR_ALL_E);                          // disable exception
    c1_status = (c1_status & (~FPU_CSR_RM)) | FPU_CSR_RN;   // set RN
    write_c1_status(c1_status);

    return ;
}


void bsp_init(void)
{
    // 硬浮点初始化
    fpu_init();

    return ;
}
#define printf (*callvec->printf)
#define gets (*callvec->gets)
#if 1
int main(int argc, char **argv, char **env, struct callvectors *cv)
{
	callvec = cv;       // 这条语句之后才能使用pmon提供的打印功能

    //bsp_init();

    // -------------------------测试gpio----------------------
    /*
     * 测试库中gpio作为输出时的相关接口
     * led闪烁10次
     */
	gpio_init(22, gpio_mode_output);
	gpio_set(22,0);
	delay_s(1);
	gpio_set(22,1);
	delay_s(1);
	gpio_set(22,0);
	delay_s(1);
	printf("\n\n***********2222*************************test start......\n");  
 //   test_gpio_output();
	printf("\n\n************************************test end......\n");  
   // test_can_general();
	/*
     * 测试库中gpio作为输入时的相关接口
     * 按键按下时，指示灯点亮，否则，熄灭
     */
   // test_gpio_input();

    /*
     * 测试库中外部中断(gpio输入中断)的相关接口
     * 按键被按下后，会产生一个中断
     */
	/*
	printf("cpu_status=0x%x\n",read_c0_status());

	exception_init();
	printf("cpu_status=0x%x\n",read_c0_status());
	// 滴答定时器初始化
	//sys_tick_init();
	printf("cpu_status=0x%x\n",read_c0_status());
	//test_gpio_key_irq();
	test_can_irq();
    // ------------------------测试PWM--------------------------------    
    // 测试硬件pwm产生连续的pwm波形
//    test_pwm_normal();

    // 测试硬件pwm产生pwm脉冲
//    test_pwm_pulse();
  */
    /*
     * 测试gpio04复用为pwm,gpio06作为普通gpio使用
     * PWM0的默认引脚位GPIO06，但也可以复用为GPIO04
     * 当gpio06还是保持默认为pwm时，复用gpio04为pwm0，那么会同时在两个引脚输出相同的pwm波形
     * 本函数旨在证明可以在gpio04复用为pwm0时，还可以将(默认作为pwm0的)gpio06作为普通gpio使用
     */
//    test_pwm_gpio04_gpio06();

    // 测试pwm最大周期
//    test_pwm_max_period();


    // ------------------------测试软件延时--------------------------------  
    // 测试延时函数delay_1ms()
//    test_delay_1ms();
    
    // 测试延时函数delay_1us()
//    test_delay_1us();
    
    // 测试延时函数delay_1s()
//    test_delay_1s();


    // ------------------------测试模拟I2C------------------------------  
    // 测试模拟I2C
//    test_simulate_i2c_am2320();


    // ------------------------测试硬件定时器---------------------------  
    // 测试硬件定时器的定时功能(读取中断状态位的方式判断是否超时)
//    test_timer_poll_time_out();

    // 测试硬件定时器的计时
//    test_timer_get_time();


    // ------------------------测试硬浮点(FPU)---------------------------
    // 测试使用硬浮点进行浮点数的加减乘除
//    test_fpu();

    // ------------------------测试硬I2C---------------------------
    // 用温湿度传感器测试硬件i2c
  //  test_i2c_am2320();

	return(0);
}

#endif




#if 0

// 调试中断的


// 打印指定地址的机器码
// 反汇编命令 mipsel-linux-objdump -S OpenLoongsonLib1c > objdump.asm
// 查看打印内容是否与反汇编一致
void print_code(void *addr)
{
    unsigned long *pfunc = (unsigned long *)addr;
    int i;
    
    printf("\func() addr = 0x%x\n", pfunc);
    for (i=0; i<10; i++)
    {
        printf("0x%x ", *pfunc);
        pfunc++;
    }
    printf("\n");
}



int main(int argc, char **argv, char **env, struct callvectors *cv)
{
	int i;
	char **ev;
	callvec = cv;

	printf("\n\n**************....\n");
	gpio_init(22, gpio_mode_output);
	gpio_set(22,0);
	delay_s(1);
	gpio_set(22,1);
	delay_s(1);
	gpio_set(22,0);
	delay_s(1);

	printf("\n\nHello start..0000000000000000....\n");
	print_code(&general_exception);
	print_code((void *)0x80000000+0x180);
	printf("\n\n******11111111111111*****************.....\n");  
	//   test_gpio_output();
	//	printf("\n\n************************************test end......\n");  
	// 初始化中断
	printf("cpu_status=0x%x\n",read_c0_status());
	// 滴答定时器初始化
	sys_tick_init();
	exception_init();


	printf("cpu_status=0x%x\n",read_c0_status());
	unsigned int c0_status = 0;
/*
	c0_status = read_c0_status();
	c0_status |= CAUSEF_IP7;
	write_c0_status(c0_status);
*/
	printf("cpu_status=0x%x\n",read_c0_status());


    // 结合反汇编查看是否memcpy成功
    print_code((void *)0x80000000+0x180);

    // 等待10s，注意观察首发版网口旁边的led是否熄灭
    for (i=0; i<2; i++)
    {
        delay_s(1);
    }

    printf("\n end...\n");
	irq_disable_all();
	return(0);
}



#endif

